<?

class MonitorManager{
	public $retry = 3;
	public $sleepMs = 2;
	public $query = array('mobile'=>'', 'hkid'=>'', 'dob'=>'', 'days'=>14);


	public $host = "";
	public $logName = "";
	public $isDebug = false;
	public $logLevel = 99; //0: no logging; 99: log all messages
	public $logLevelMin = 10; //1: All, which level smaller than this value will not log
	public $pathPrefix = '';
	public $processLogPath = "data/processLog.txt";
	public $datasetDir = "data/database/";
	public $database = NULL;
	public $mailData = array();

	public $_cURL = NULL;
	public $_htmlDom = NULL;

	public function __construct($host, $logName, $query = array(), $pathPrefix = null){
		$this->host		= $host;
		$this->logName	= $logName;

		if( empty($pathPrefix) )
			$pathPrefix		= dirname( __FILE__ ) . '/';
		$this->processLogPath	= $pathPrefix . $this->processLogPath;
		$this->datasetDir		= $pathPrefix . $this->datasetDir;

		$this->_loadDatabase();
		$this->_cURL = curl_init();
		$this->_htmlDom = new simple_html_dom();

		$opts = array('mobile', 'hkid', 'dob', 'days');
		foreach( $opts as $opt ){
			if( isset( $query[ $opt ] ) )
				$this->query[ $opt ] = $query[ $opt ];
		}

	}

	function __destruct() {
		curl_close($this->_cURL);
		$this->_htmlDom->clear();
	}

	public function process( $autoBook = false){
		$cookieStr = 'PHPSESSID=' . session_id(); //IMPORTANT SESSION ID
		$successLogin = $this->doLogin($cookieStr);

		if( !$successLogin ){
			$this->_log("Login Fail, process stopped. process();", 70);
			return '';
		}

		$html4Proc4 = $this->getRemoteData( $this->host . '/manageStudentLesson/bookLesson', $callParam3, $cookieStr);

		$retry = $this->retry;
		while( empty( $this->mailData ) && $retry >= 0){
			if( $retry != $this->retry ){
				$this->_log("Extra empty timeslot; Retry: " . ($this->retry - $retry) . ";", 30);
				usleep( $this->sleepMs * 1000 );
			}

			$retry--;
			$callParam4 = array('currentDate'=>date("Ymd"), 'currentDateEnd'=>date("Ymd", strtotime("+" . $this->query['days'] . " days")));
			$html4Proc5 = $this->getRemoteData( $this->host . '/manageStudentLesson/bookGLPracLessonData', $callParam4, $cookieStr);

			$bookingTimeSlot = json_decode($html4Proc5['body'], 1);
			$this->mailData = $this->getExtraBookingInfo($bookingTimeSlot);
			
		}
		
		if( $autoBook ):
			$autoBooked = $this->bookLessions();
		endif;

		$this->_saveDatabase( $this->mailData );
		$mailContent = $this->prepareMailContent( $autoBooked );
		return $mailContent;
	}

	public function prepareMailContent($autoBooked = null){
		//return a mail content
		if( $this->database == $this->mailData )
			return '';

		$rtn = '';
		$lastDate = "";
		if( !empty($autoBooked) ){
			$i = 0;
			foreach($autoBooked as $timeData){
				if( $timeData['serv_date_d'] != $lastDate){
					$rtn .= ( empty($rtn) ? "" : "</span>" );
					$rtn .= "<h2 style='color:red'>AutoBook: {$timeData['serv_date_d']} ({$timeData['weekday']})</h2><span style='color:red'>";
				}

				$rtn .= (( $timeData['serv_date_d'] != $lastDate) ? "" : ", " ) . $timeData['slot_time'];
				$lastDate = $timeData['serv_date_d'];
			}
			$rtn .= "</span>";
		}
		
		//find the different
		$lastDate = "";
		foreach($this->mailData as $timeData){
			$timeslotExist = false;
			foreach($this->database as $timeDataDB){
				if( $timeData['serv_date_d']==$timeDataDB['serv_date_d'] && $timeData['slot_time']==$timeDataDB['slot_time'] ){
					$timeslotExist = true;
					break;
				}
			}
			if( !$timeslotExist ){
				if( $timeData['serv_date_d'] != $lastDate)
					$rtn .= ( empty($rtn) ? "" : "<hr />" ) . "<h2>{$timeData['serv_date_d']} ({$timeData['weekday']})</h2>";

				$rtn .= (( $timeData['serv_date_d'] != $lastDate) ? "" : ", " ) . $timeData['slot_time'];
				$lastDate = $timeData['serv_date_d'];
			}
		}

		if( !empty($rtn) )
			$rtn	.= "<hr/><br />以上資料僅供參考";
		return $rtn;
		//return $rtn . "<br /><br /><pre>" . print_r( $this->mailData, 1 ) . "</pre>";
	}

	public function bookLessions(){
		$ts = $this->mailData;
		$tsCnt = count($ts);
		$autoBooked = array();
		
		if( $tsCnt < 2 ):
			$this->_log("AutoBook is stopped, there is ONE timeslot;", 70);
			return $autoBooked;
		endif;
	
		for($i=0; $i<$tsCnt; $i++){
			$j = $i + 1;
			if( $j < $tsCnt && $ts[$i]['serv_date_d'] == $ts[$j]['serv_date_d'] && $ts[$i]['end_time'] == $ts[$j]['start_time'] ){
					$valid = false; //FollowTimeSlots are Valid???
					//http://www.td.gov.hk/tc/public_services/licences_and_permits/driving_test/driving_test_of_noncommercial_vehicles_/
					//學習駕駛執照持有人應遵守的規條
					switch( $ts[$i]['weekday'] ){
						case 'Sun':
							$valid = true;
							break;
						case 'Sat':
							$valid = ($ts[$i]['start_time'] >= '09:30');
							break;
						default:
							$valid = ($ts[$i]['start_time'] >= '19:30' || ($ts[$i]['start_time'] >= '09:30' && $ts[$j]['end_time'] <= '16:30'));
							break;
					}
					
					$this->_log("AutoBook::: valid: " . ($valid?'TRUE':'false') . "; i: {$i}; j: {$j}; Date: {$ts[$i]['serv_date_d']}; Times: {$ts[$i]['start_time']}, {$ts[$j]['start_time']};", 10);
					
					if( $valid ){
						//current item and next item in same date & they are following
						$this->_log("AutoBook::: i: {$i}; j: {$j}; {$ts[$i]['serv_date_d']}: {$ts[$i]['start_time']}, {$ts[$j]['start_time']};", 70);
						$result = $this->doBookTimeSlot(  $ts[$i]  );
						if($result){
							array_push($autoBooked, $ts[$i]);
							$result = $this->doBookTimeSlot(  $ts[$j]  );
							if($result){
								array_push($autoBooked, $ts[$j]);
								$i++;
								break;
							}
						}
					}
			}
		}
		
		return $autoBooked;
	}

	public function getExtraBookingInfo( $free_time_slot ){
		$booking = array();
		if( empty($free_time_slot) )
			return $booking;

		foreach($free_time_slot as $date => $dateData){
			foreach($dateData as $timeData){
				if( $timeData['LG_CanBook'] == 'Y'){
					$itm = array();
					$itm['serv_date_d']		= $timeData['serv_date_d'];
					$itm['slot_time']		= $timeData['slot_start_time'] . ' - ' . $timeData['slot_end_time'];
					$itm['start_time']		= $timeData['slot_start_time'];
					$itm['end_time']		= $timeData['slot_end_time'];
					$itm['weekday']			= date("D", strtotime($timeData['serv_date_d']));
					$itm['LG_Capacity']		= $timeData['LG_Capacity'];
					$itm['LG_Book']			= $timeData['LG_Book'];
					$itm['LG_Avail']		= $timeData['LG_Avail'];
					$itm['road_restricted']	= $timeData['road_restricted'];
					array_push($booking, $itm);
				}
			}
		}
		return $booking;
	}

	/////////////////////////////////////////////////////
	//     Not recommended to edit below this line     //
	/////////////////////////////////////////////////////

	private function doBookTimeSlot($bookingDetail){
		//function does not finish
		$bookingParam = array('confirm_code'=>'', 'company'=>'HK', 'course_class'=>'FU', 'veh_type'=>'LG', 'model'=>'NL', 'lesson_type'=>'E', 'product_code'=>'PRAC', 'type_code'=>'I', 'lesson_centre'=>'HK', 'language_code'=>'C');
		$bookingParam['confirm_code']	= $this->query['hkid']; //your ID Card
		$bookingParam['bk_no']			= ''; //leave blank
		$bookingParam['bk_date']		= $bookingDetail['serv_date_d']; // '2014-04-01'
		$bookingParam['bk_from_time']	= $bookingDetail['start_time']; // '13:30'
		$bookingParam['ajax']			= ''; //leave blank
		$html4Booking = $this->getRemoteData( $this->host . '/manageStudentLesson/bookGLPracLessonSubmit', $bookingParam, $cookieStr);
		return 'Y' == strtoupper($html4Booking['body']) ? true : false;
		/*
		$bookingParam = array('confirm_code'=>'', 'company'=>'HK', 'course_class'=>'FU', 'veh_type'=>'LG', 'model'=>'NL', 'lesson_type'=>'E', 'product_code'=>'PRAC', 'type_code'=>'I', 'lesson_centre'=>'HK', 'language_code'=>'C');
		$bookingParam['confirm_code']	= $this->query['hkid']; //your ID Card
		$bookingParam['bk_no']			= ''; //leave blank
		$bookingParam['bk_date']		= '2014-04-01'; //serv_date_d
		$bookingParam['bk_from_time']	= '13:30'; //slot_start_time
		$bookingParam['ajax']			= ''; //leave blank

		POST >> http://hksm.meghk.com/web/wcs/manageStudentLesson/bookGLPracLessonSubmit
		RETURN >> N | Y: OK
		*/
	}

	private function doLogin($cookieStr, $retry = 1 ){
		$saveResponse = "";
		$callParam1 = array('code2'=>$this->query['mobile'], 'user_id'=>$this->query['hkid'], 'code1'=>$this->query['dob']);

		$html4Proc1 = $this->getRemoteData( $this->host . '/manageLogin/loginSubmit', $callParam1, $cookieStr);
		if( $this->isUnderMaintenance($html4Proc1) ){
			$this->_log("Website under maintenance; redirect_url: {$html4Proc1['redirect_url']};", 50);
			return false;
		}

		$html4Proc2 = $this->getRemoteData( $this->host . '/manageGeneral/student_profile', null, $cookieStr);
		$callParam2 = $this->extraParam2($html4Proc2['body']);

		if( empty($callParam2) ){
			$this->_log("Login Fail, retry remain: {$retry};", 30);

			$saveResponse .= "BODY (Process 2)>>\n" . $html4Proc2['body'];
			unset($html4Proc2['body']);
			$saveResponse .= "\n\n\ncURL (Process 2) Return>>\n" . print_r($html4Proc2, 1);

			$this->_saveResponse($saveResponse);

			$retry--;
			if( $retry >= 0 ){
				usleep( $this->sleepMs * 1000 );
				return $this->doLogin($cookieStr, $retry);
			}
		}

		$html4Proc3 = $this->getRemoteData( $this->host . '/manageStudentLesson/studentLessonList', $callParam2, $cookieStr);
		$callParam3 = $this->extraParam3($html4Proc3['body']);

		if( empty($callParam3) && $callParam3 !== FALSE ){
			$this->_log("Login Fail, retry remain: {$retry};", 30);
			
			$saveResponse .= "BODY (Process 3)>>\n" . $html4Proc3['body'];
			unset($html4Proc3['body']);
			$saveResponse .= "\n\n\ncURL (Process 3) Return>>\n" . print_r($html4Proc3, 1);

			$this->_saveResponse($saveResponse);

			$retry--;
			if( $retry >= 0 ){
				usleep( $this->sleepMs * 1000 );
				return $this->doLogin($cookieStr, $retry);
			}
			return false;
		}else
			return true;
	}

	private function extraParam2( $html ){
		$output = array();

		$this->_htmlDom->load($html);
		$formObjs = $this->_htmlDom->find( "ul.data-listview li.link" );
		$this->_log("extraParam2(): finding FORMs", 1);

		foreach($formObjs as $formObj){
			$lessonAvailable = $formObj->find("div#labelVal", 0);
			if( !empty($lessonAvailable) ){
				$lessonAvailable = $lessonAvailable->innertext;
				$lessonAvailable = preg_replace('/[^\d]/', '', $lessonAvailable);

				if( $lessonAvailable > 0 ){
					$inputs = $formObj->find("input[type!=button]");
					foreach($inputs as $input)
						$output[ $input->name ] = $input->value;
					break;
				}
			}
		}
		$this->_htmlDom->clear();
		return $output;
	}

	private function extraParam3( $html ){
		$output = array();

		$this->_htmlDom->load($html);
		$formObjs = $this->_htmlDom->find( "ul[class=unfinish_lesson data-listview] li" );
		$this->_log("extraParam3(): finding FORMs", 1);

		foreach($formObjs as $formObj){
			$hasUnbookLesson = $formObj->find("div[class=lesson_title unbook]", 0);
			if( !empty($hasUnbookLesson) ){
				$inputs = $formObj->find("input[type!=button]");
				foreach($inputs as $input)
					$output[ $input->name ] = $input->value;
				break;
			}else{
				$this->_htmlDom->clear();
				$this->_log("extraParam3(): there are no unbook; (div[class=lesson_title unbook])", 20);
				return FALSE;
			}
		}
		$this->_htmlDom->clear();
		return $output;
	}

	private function isUnderMaintenance( $cURL = array() ){
		if( !isset($cURL['redirect_url']) )
			return false;
		else
			return ($cURL['redirect_url'] == $this->host . '/managePage/maintenance') || (preg_match("/maintenance/", $cURL['redirect_url']) == 1);
	}

	public function getRemoteData($url, $data = NULL, $cookieStr = '', $method = 'POST', $customHeader = NULL){
		//download the remote data - cURL
		$retry = $this->retry;
		$sleepMs = $this->sleepMs;

		curl_setopt($this->_cURL, CURLOPT_SSL_VERIFYHOST, 1);
		curl_setopt($this->_cURL, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($this->_cURL, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($this->_cURL, CURLOPT_HEADER, true);
		curl_setopt($this->_cURL, CURLOPT_VERBOSE, false);
		curl_setopt($this->_cURL, CURLOPT_USERAGENT, 'Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.1; .NET CLR 2.0.50727; .NET CLR 3.0.04506.30; InfoPath.1)');

		if( !empty($cookieStr) )
			curl_setopt($this->_cURL, CURLOPT_COOKIE, $cookieStr);

		if( !empty($customHeader) )
			curl_setopt($this->_cURL, CURLOPT_HTTPHEADER, $customHeader);

		if( !empty($data) ){
			if('POST' == strtoupper($method)){
				//POST
				curl_setopt($this->_cURL, CURLOPT_POST, true);
				curl_setopt($this->_cURL, CURLOPT_POSTFIELDS, $data);
			}else{
				//GET
				curl_setopt($this->_cURL, CURLOPT_POST, false);
				$url .= ( strpos($url, '?')===FALSE ? '?' : '&' ) . http_build_query($data);
			}
		}
		curl_setopt($this->_cURL, CURLOPT_URL, $url);
		$rtn = curl_exec( $this->_cURL );

		while ( $retry > 0 && $rtn === FALSE ) {
			$retry--;
			usleep( $sleepMs * 1000 );
		$rtn = curl_exec( $this->_cURL );
		}

		$logUrlStr = str_replace($this->host, "", $url);
		$this->_log("Response len: " . strlen($rtn) . "; Retry: " . ($this->retry - $retry) . "; URL: {$logUrlStr};", 10);

		$tmp = curl_getinfo($this->_cURL);
		$tmp['body'] = substr($rtn, $tmp['header_size']);
		//$tmp['head'] = $this->_http_parse_headers(substr($rtn, 0, $tmp['header_size']));

		return $tmp;
	}

	private function _http_parse_headers ($raw_headers) {
		$headers = array();
		foreach (explode("\n", $raw_headers) as $i => $h){
			$h = explode(':', $h, 2);
			if (isset($h[1]))
				$headers[$h[0]] = trim($h[1]);
		}
		return $headers;
	}

	public function _decodeJsonFile($path, $defValOnErr = NULL){
		//decode the json file to array, return array || $defValOnErr
		if( file_exists($path) === FALSE ){
			return $defValOnErr;
		}else{
			$content = file_get_contents($path);
			if ($content == "")
				return $defValOnErr;
			else
				return json_decode($content, true);
		}
	}

	public function _encodeJsonFile($path, $data){
		//encode the data to json format and save to file
		$json = empty($data) ? '[]' : json_encode($data) ;
		return file_put_contents($path, $json);
	}

	public function _loadDatabase(){
		$databasePath = $this->datasetDir . $this->logName . ".txt";
		$data = $this->_decodeJsonFile($databasePath);
		$this->database = $data;
	}

	public function _saveDatabase( $saveData = "" ){
		$databasePath = $this->datasetDir . $this->logName . ".txt";
		$result = $this->_encodeJsonFile($databasePath, $saveData);
		$chmodR = @chmod($databasePath, 0644);
		$this->_log("Saved to db; bytes wroten: {$result};", 10);
	}

	public function _saveResponse( $saveData = "" ){
		$databasePath = $this->datasetDir . date("Ymd_His") . ".txt";
		$result = file_put_contents($databasePath, $saveData);
		$chmodR = @chmod($databasePath, 0644);
		$this->_log("Saved Response; bytes wroten: {$result}; Saved to: {$databasePath};", 10);
	}

	public function debugMode($mode = true){
		$this->isDebug = $mode;
	}

	public function loggingMode($logLevel = 0, $logLevelMin = 20){
		$this->logLevel = $logLevel;
		$this->logLevelMin = $logLevelMin;
	}

	public function _log($msg, $msgLevel = '1', $forcePrint = false){
		$msg = $this->_getLogCode($msgLevel) . $msg;
		if($this->logLevel > 0 && $this->logLevel >= $msgLevel && $msgLevel >= $this->logLevelMin)
			@file_put_contents($this->processLogPath, date("Y-m-d H:i:s ") . $msg . "\n", FILE_APPEND);

		if($this->isDebug || $forcePrint)
			print(date("Y-m-d H:i:s ") . " --" . $msg . "<br />\n");
	}

	private function _getLogCode( $level ){
		$lvMapping = array();
		$lvMapping[1]	= "[Debu]";
		$lvMapping[10]	= "[Info]";
		$lvMapping[20]	= "[Noti]";
		$lvMapping[30]	= "[Warn]";
		$lvMapping[40]	= "[Erro]";
		$lvMapping[50]	= "[Crit]";
		$lvMapping[70]	= "[Aler]";
		$lvMapping[90]	= "[Emer]";
		return isset($lvMapping[ $level ]) ? $lvMapping[ $level ] . " ({$level}) " : '[Unkn]';
	}

}

?>
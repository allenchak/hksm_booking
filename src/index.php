<?php
require_once("NotifyManager.php");
require_once("simple_html_dom.php");
require_once("HKSM_Monitor_Manager.php");


date_default_timezone_set('Asia/Hong_Kong');

$hksmQuery = array('mobile'=>'98765432' /*Mobile*/, 'hkid'=>'Z1234' /* HKID */, 'dob'=>'1231' /* DOB; MMDD */);
$pathPrefix = dirname( __FILE__ ) . '/';
$mm = new MonitorManager('http://hksm.meghk.com/web/wcs', 'hksm', $hksmQuery, $pathPrefix);

if( date("H:i") >= "23:00" || date("H:i") <= "07:59" ){
	$mm->_log('Website is offlined!', 1);
	die();
}

$mailContent = $mm->process();
$mailContent .= (!isset($_GET['test'])) ? '' : 'I am <u><b>tesing</b></u>.';
print("<hr>{$mailContent};<hr>");


if( !empty($mailContent) ){
		$dateLink	= 'http://' . $_SERVER['HTTP_HOST'] . dirname( $_SERVER['SCRIPT_NAME'] ) . '/date.php' ;
		$mailContent .= "<br /><br /><a href='{$dateLink}'>Date View</a> | <a href='http://hksm.meghk.com/web/wcs'>HKSM Login</a>";
		$tos	= 'youremail@gmail.com';
		$nm		= new NotifyManager();
		$mailSubject	= '香港駕駛學院 - 預約課堂';
		
		//send Boxcar2 (Push)
		$result1 = $nm->sendPush_Boxcar2("YOUR_TOKEN", $mailSubject, $mailContent, array('notification[sound]'=>'done'));
		
		//send Mail (using PHP)
		//$result = $nm->sendMail($tos, $mailSubject, $mailContent);
		
		//send Mail (using Zend_Mail)
		$extra = array(
			'customHeader'	=> array('codeVersion'=>'4.2', 'codeHost'=>'http://' . $_SERVER["HTTP_HOST"] . $_SERVER["REQUEST_URI"], 'X-Mailer'=>'PHP/5'),
			'replyTo'		=> $tos
				);
		//$result = $nm->sendMail_Zend($tos, $mailSubject, $mailContent, $extra);
} 

?>
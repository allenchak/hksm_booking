<?php

class NotifyManager{
	public $isDebug = true;
	public $logging = false;
	public $processLogPath = "/data/processLog.txt";
	public $cookiePath = "/data/cookie.txt";
	public $_cURL = NULL;
	public $attachments = NULL;
	
	public function dumpinfo(){
		print("<pre>");
		/** /
		print("\nhost: ");var_dump($this->host);
		//var_dump($this);
		/**/
		print("</pre>");
	}

	public function __construct(){
		$this->processLogPath = dirname( __FILE__ ) . $this->processLogPath;
		$this->cookiePath = dirname( __FILE__ ) . $this->cookiePath;
	}
	
    function __destruct() {
	}
	
	
	public function debugMode($mode = true){
		//set to debug mode
		$this->isDebug = $mode;
	}
	
	public function loggingMode($mode = true){
		//set to debug mode
		$this->logging = $mode;
	}
	
	public function _log($msg, $forcePrint = false){
		//write to log
		if($this->logging)
			@file_put_contents($this->processLogPath, date("Y-m-d H:i:s ") . " -[{$this->logName}]- " . $msg . "\n", FILE_APPEND);
		
		if($this->isDebug || $forcePrint)
			print(date("Y-m-d H:i:s ") . " -- " . $msg . "<br />\n");
	}
	
	private function _http_parse_headers ($raw_headers) {
		$headers = array();
		foreach (explode("\n", $raw_headers) as $i => $h){
			$h = explode(':', $h, 2);
			if (isset($h[1]))
				$headers[$h[0]] = trim($h[1]);
		}
		return $headers;
	}
	
	public function addMailAttachment($dataType, $file){
		//dataType = file path | resource (STDIN)
		//$file = array('path'=>"@/abc.pdf", 'mime'=>"application/pdf", 'filename'=>"displayFileName.pdf", 'resource'=>$binaryString);
		//$this->attachments
	}
	
	public function sendMail_Zend($tos, $subject, $body, $extra = array()){
		require_once("Zend/Mail.php");
		$mail = new Zend_Mail('UTF-8');
			$mail->addHeader('X-Mailer:', 'PHP/'.phpversion());
			$mail->setSubject( $subject );
			$mail->setBodyHtml( $body );
			
			if( is_array($tos) ){
				foreach( $tos as $to )
					$mail->addTo( $to );
			}else
				$mail->addTo($tos);
			
			//cc / bcc
			if( isset($extra['cc']) ){
				if( is_array($extra['cc']) ){
					foreach( $extra['cc'] as $cc )
						$mail->addCc( $cc );
				}else
					$mail->addCc( $extra['cc'] );
			}
			if( isset($extra['bcc']) ){
				if( is_array($extra['bcc']) ){
					foreach( $extra['bcc'] as $bcc )
						$mail->addBcc( $bcc );
				}else
					$mail->addBcc( $extra['bcc'] );
			}
			
			//add custom header
			if( isset($extra['customHeader']) ){
				foreach( $extra['customHeader'] as $hKey=>$hVal )
					$mail->addHeader($hKey, $hVal);
			}
			if( isset($extra['replyTo']) ){
				$mail->setReplyTo($extra['replyTo']);
			}
			
			//attachments
			//http://framework.zend.com/manual/1.11/en/zend.mail.attachments.html
			if( !empty($this->attachments) ){
				foreach( $this->attachments as $a ){
					$tmp = $mail->createAttachment( $a['resource'] ); //$pdfBinary
					$tmp->filename		= $a['filename'];
					$tmp->type			= $a['mime'];
					//$tmp->disposition	= Zend_Mime::DISPOSITION_INLINE;
					//$tmp->encoding		= Zend_Mime::ENCODING_BASE64;;
				}
			}
			
		$mail->send();
		return true;
	}
	
	public function sendMail($tos, $subject, $body, $extra = array()){
		// multiple recipients
		$to = is_array($tos) ? implode(", ", $tos) : $tos;

		// To send HTML mail, the Content-type header must be set
		$headers  = 'MIME-Version: 1.0' . "\r\n";
		$headers .= 'Content-type: text/html; charset=UTF-8' . "\r\n";

		// Additional headers
		if( isset($extra['cc']) )
			$headers .= 'Cc: ' . (is_array($extra['cc']) ? implode(", ", $extra['cc']) : $extra['cc']) . "\r\n";
		if( isset($extra['bcc']) )
			$headers .= 'Bcc: ' . (is_array($extra['bcc']) ? implode(", ", $extra['bcc']) : $extra['bcc']) . "\r\n";

		// Mail it
		@mail($to, $subject, $body, $headers);
		return true;
	}
	
	public function sendPush_Boxcar2($tos, $subject, $body, $extra = array()){
		//send Boxcar2 (Push)
		// API Documentation: http://boxcar.uservoice.com/knowledgebase/articles/306788
		$url	= "https://new.boxcar.io/api/notifications";
		$data	= array("user_credentials" => $tos, "notification[title]" => mb_substr($subject, 0, 255, 'UTF-8'), "notification[long_message]" =>  mb_substr($body, 0, 4096, 'UTF-8'), "notification[source_name]" => "");
		
		$allowExtraData	= array('notification[sound]', 'notification[source_name]');
		if( !empty($extra) ){
			foreach( $extra as $eKey=>$eVal ){
				if( in_array($eKey, $allowExtraData) )
					$data[ $eKey ] = $eVal;
			}
		}
		
		$cURL = curl_init();
		curl_setopt($cURL, CURLOPT_SSL_VERIFYHOST, 1);
		curl_setopt($cURL, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($cURL, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($cURL, CURLOPT_HEADER, true);
		curl_setopt($cURL, CURLOPT_USERAGENT, 'Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.1; .NET CLR 2.0.50727; .NET CLR 3.0.04506.30; InfoPath.1)');
		curl_setopt($cURL, CURLOPT_POST, true);
		curl_setopt($cURL, CURLOPT_POSTFIELDS, $data);
		curl_setopt($cURL, CURLOPT_URL, $url);
		$rtn = curl_exec( $cURL );
		
		$tmp = curl_getinfo($cURL);
		$tmp['body'] = substr($rtn, $tmp['header_size']);
		$head = $this->_http_parse_headers(substr($rtn, 0, $tmp['header_size']));
		//$tmp['head'] = $head;
		
		return $tmp;
	}
	
	public function sendPush_Boxcar($apiKey, $subject, $body, $extra = array(), $apiSecret = ''){
		//send Boxcar (Push)
		// API Documentation: http://boxcar.io/help/api/providers
		$url	= "https://boxcar.io/devices/providers/" . $apiKey . "/notifications/";
		
		$data = array(
								'token'								=> $apiKey,
								'secret'							=> $apiSecret,
								'email'								=> $tos,
								'notification[from_screen_name]'	=> $subject,
								'notification[message]'				=> $body
							);
		
		$allowExtraData	= array('notification[from_remote_service_id]', 'notification[redirect_payload]', 'notification[source_url]', 'notification[icon_url]');
		if( !empty($extra) ){
			foreach( $extra as $eKey=>$eVal ){
				if( in_array($eKey, $allowExtraData) )
					$data[ $eKey ] = $eVal;
			}
		}
		
		$cURL = curl_init();
		curl_setopt($cURL, CURLOPT_SSL_VERIFYHOST, 1);
		curl_setopt($cURL, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($cURL, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($cURL, CURLOPT_HEADER, true);
		curl_setopt($cURL, CURLOPT_USERAGENT, 'UKD1_Boxcar_Client');
		curl_setopt($cURL, CURLOPT_POST, true);
		curl_setopt($cURL, CURLOPT_POSTFIELDS, $data);
		curl_setopt($cURL, CURLOPT_URL, $url);
		$rtn = curl_exec( $cURL );
		
		$tmp = curl_getinfo($cURL);
		$tmp['body'] = substr($rtn, $tmp['header_size']);
		$head = $this->_http_parse_headers(substr($rtn, 0, $tmp['header_size']));
		//$tmp['head'] = $head;
		
		return $tmp;
	}
	
	public function sendPush_PushBullet($tos, $subject, $body, $extra = array(), $apiKey = ''){
		//send PushBullet (Push)
		// API Documentation: https://www.pushbullet.com/api
		$url		= "https://api.pushbullet.com/api/pushes";
		$data		= array("device_iden" => $tos, "type" => 'note', "title" => $subject, "body" => $body);
		//$data		= array("device_iden" => $tos, "type" => 'link', "title" => $subject, "url" => $body);
		//$data		= array("device_iden" => $tos, "type" => 'address', "name" => $subject, "address" => $body);
		//$data		= array("device_iden" => $tos, "type" => 'list', "title" => $subject, "items" => $body);
		//$data		= array("device_iden" => $tos, "type" => 'file', "file" => $body);
		
		$allowExtraData	= array('notification[from_remote_service_id]', 'notification[redirect_payload]', 'notification[source_url]', 'notification[icon_url]');
		if( !empty($extra) ){
			foreach( $extra as $eKey=>$eVal ){
				if( in_array($eKey, $allowExtraData) )
					$data[ $eKey ] = $eVal;
			}
		}
		
		$cURL = curl_init();
		curl_setopt($cURL, CURLOPT_SSL_VERIFYHOST, 1);
		curl_setopt($cURL, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($cURL, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($cURL, CURLOPT_HEADER, true);
		curl_setopt($cURL, CURLOPT_USERAGENT, 'Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.1; .NET CLR 2.0.50727; .NET CLR 3.0.04506.30; InfoPath.1)');
		curl_setopt($cURL, CURLOPT_POST, true);
		curl_setopt($cURL, CURLOPT_POSTFIELDS, $data);
		curl_setopt($cURL, CURLOPT_URL, $url);
		curl_setopt($cURL, CURLOPT_USERPWD, $apiKey); //for PushBullet
		$rtn = curl_exec( $cURL );
		
		$tmp = curl_getinfo($cURL);
		$tmp['body'] = substr($rtn, $tmp['header_size']);
		$head = $this->_http_parse_headers(substr($rtn, 0, $tmp['header_size']));
		//$tmp['head'] = $head;
		
		return $tmp;
	}
	
	public function sendPush_PushCo($apiKey, $subject, $body, $extra = array(), $apiSecret = ''){
		//send Push.Co (Push)
		// API Documentation: http://push.co/api/push
		$url		= "https://api.push.co/1.0/push";
		$data		= array("api_key" => $apiKey, "message" => $subject, "article" => $body, "api_secret" => $apiSecret);
		
		$allowExtraData	= array('view_type', 'notification_type', 'image', 'url', 'latitude', 'longitude');
		if( !empty($extra) ){
			foreach( $extra as $eKey=>$eVal ){
				if( in_array($eKey, $allowExtraData) )
					$data[ $eKey ] = $eVal;
			}
		}
		
		$cURL = curl_init();
		curl_setopt($cURL, CURLOPT_SSL_VERIFYHOST, 1);
		curl_setopt($cURL, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($cURL, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($cURL, CURLOPT_HEADER, true);
		curl_setopt($cURL, CURLOPT_USERAGENT, 'Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.1; .NET CLR 2.0.50727; .NET CLR 3.0.04506.30; InfoPath.1)');
		curl_setopt($cURL, CURLOPT_POST, true);
		curl_setopt($cURL, CURLOPT_POSTFIELDS, $data);
		curl_setopt($cURL, CURLOPT_URL, $url);
		$rtn = curl_exec( $cURL );
		
		$tmp = curl_getinfo($cURL);
		$tmp['body'] = substr($rtn, $tmp['header_size']);
		$head = $this->_http_parse_headers(substr($rtn, 0, $tmp['header_size']));
		//$tmp['head'] = $head;
		
		return $tmp;
	}
	
	public function sendPush_InstaPush($appId, $subject, $body, $extra = array(), $apiSecret = ''){
		//send InstaPush (Push)
		// API Documentation: https://instapush.im/developer/rest#send
		$url		= "https://api.instapush.im/v1/post";
		$data		= array("event" => "booking", "trackers" => array("timeslots" => $subject . $body));
		
		$cURL = curl_init();
		curl_setopt($cURL, CURLOPT_SSL_VERIFYHOST, 1);
		curl_setopt($cURL, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($cURL, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($cURL, CURLOPT_HEADER, true);
		curl_setopt($cURL, CURLOPT_USERAGENT, 'Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.1; .NET CLR 2.0.50727; .NET CLR 3.0.04506.30; InfoPath.1)');
		curl_setopt($cURL, CURLOPT_POST, true);
		curl_setopt($cURL, CURLOPT_POSTFIELDS, json_encode($data) );
		curl_setopt($cURL, CURLOPT_URL, $url);
		curl_setopt($cURL, CURLOPT_HTTPHEADER, array("x-instapush-appid: " . $appId, "x-instapush-appsecret: " . $apiSecret)); //for InstaPush
		$rtn = curl_exec( $cURL );
		
		$tmp = curl_getinfo($cURL);
		$tmp['body'] = substr($rtn, $tmp['header_size']);
		$head = $this->_http_parse_headers(substr($rtn, 0, $tmp['header_size']));
		//$tmp['head'] = $head;
		
		return $tmp;
	}
	
	public function sendPush_XNotify($apiKey, $subject, $body, $extra = array(), $apiSecret = ''){
		//send XNotify (Push)
		// SITE: https://www.extendedpush.com/notificationList.php
		/*
			notifyEvents:3
			groupName:50
			notifyMsg:big message
			schedule_datatime:04/08/2014 11:20 AM
			schedule_label:1
			label_id:0
			hidden_news_filename:2014_3_8_1396927340851_2933.png
			news_header:Big Head
			color_pic_header:E15A5F
			news_content:big gun with a big man <hr /> AWESOME
			news_footer:BIG FOOT
			color_pic_footer:E15A5F
			html_content:<link href="styles.css" rel="stylesheet" type="text/css" /><head><meta http-equiv="Content-Type" content="text/html; charset=utf-8" /><meta name="viewport" content="initial-scale=1, maximum-scale=1"></head><div class="header_prev" style="background-color:rgb(225, 90, 95)"><div class="logo"><img src="https://www.extendedpush.com/uploads/newsLetters/logo/default_logo.png"/></div><div class="seprater"></div><div class="headertxt">Big Head</div></div><div class="content">big gun with a big man &lt;hr /&gt; AWESOME</div><div class="footer" style="background-color:rgb(225, 90, 95);margin-left:-14px;">BIG FOOT</div>
			session_id:53
		*/
		
		return $tmp;
	}
	
	public function sendPush_Kayac($tos, $subject, $body, $extra = array()){
		//send Push.Co (Push)
		// API Documentation: http://im.kayac.com/#docs
		$url		= "http://im.kayac.com/api/post/" . $tos;
		$data		= array("message" => $subject . $body);
		
		$allowExtraData	= array('handler', 'password', 'sig');
		if( !empty($extra) ){
			foreach( $extra as $eKey=>$eVal ){
				if( in_array($eKey, $allowExtraData) )
					$data[ $eKey ] = $eVal;
			}
		}
		
		$cURL = curl_init();
		curl_setopt($cURL, CURLOPT_SSL_VERIFYHOST, 1);
		curl_setopt($cURL, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($cURL, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($cURL, CURLOPT_HEADER, true);
		curl_setopt($cURL, CURLOPT_USERAGENT, 'Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.1; .NET CLR 2.0.50727; .NET CLR 3.0.04506.30; InfoPath.1)');
		curl_setopt($cURL, CURLOPT_POST, true);
		curl_setopt($cURL, CURLOPT_POSTFIELDS, $data);
		curl_setopt($cURL, CURLOPT_URL, $url);
		$rtn = curl_exec( $cURL );
		
		$tmp = curl_getinfo($cURL);
		$tmp['body'] = substr($rtn, $tmp['header_size']);
		$head = $this->_http_parse_headers(substr($rtn, 0, $tmp['header_size']));
		//$tmp['head'] = $head;
		
		return $tmp;
	}
	
	public function sendPush_PushIo($tos, $subject, $body, $extra = array()){
		$tmp = array();
		$tmp['error']	= "Feature not available";
		$tmp['tos']		= $tos;
		$tmp['subject']	= $subject;
		$tmp['body']	= $body;
		$tmp['extra']	= $extra;
		
		return $tmp;
	}
	
	public function sendPush_PushOver($tos, $subject, $body, $extra = array()){
		$tmp = array();
		$tmp['error']	= "Feature not available";
		$tmp['tos']		= $tos;
		$tmp['subject']	= $subject;
		$tmp['body']	= $body;
		$tmp['extra']	= $extra;
		
		return $tmp;
	}
	
	public function sendPush_Whatsapp($tos, $subject, $body, $extra = array()){
		$tmp = array();
		$tmp['error']	= "Feature not available";
		$tmp['tos']		= $tos;
		$tmp['subject']	= $subject;
		$tmp['body']	= $body;
		$tmp['extra']	= $extra;
		
		return $tmp;
	}
	
	public function sendPush_Line($tos, $subject, $body, $extra = array()){
		$tmp = array();
		$tmp['error']	= "Feature not available";
		$tmp['tos']		= $tos;
		$tmp['subject']	= $subject;
		$tmp['body']	= $body;
		$tmp['extra']	= $extra;
		
		return $tmp;
	}
	
	public function sendPush_Hangouts($tos, $subject, $body, $extra = array()){
		$tmp = array();
		$tmp['error']	= "Feature not available";
		$tmp['tos']		= $tos;
		$tmp['subject']	= $subject;
		$tmp['body']	= $body;
		$tmp['extra']	= $extra;
		
		return $tmp;
	}
	
	public function sendPush_WeChat($tos, $subject, $body, $extra = array()){
		$tmp = array();
		$tmp['error']	= "Feature not available";
		$tmp['tos']		= $tos;
		$tmp['subject']	= $subject;
		$tmp['body']	= $body;
		$tmp['extra']	= $extra;
		
		return $tmp;
	}

	
}


/** /
- main()
- send()
- addMailAttachment()
@fileContent: mime-type, resource, filename, data-type(STDIN/path)
- sendMail()
- sendMail_Zend()
- sendPush() [private]
@$cURL
@$customHeader
>return StatusCode
- sendPush_Boxcar2() //OK
- sendPush_Boxcar() //OK
- sendPush_PushBullet() //OK
- sendPush_PushCo() //OK, FOR iOS 6+
- sendPush_InstaPush() //OK
- sendPush_XNotify() //OK, iOS 6+, Android
- sendPush_Kayac() //OK, FOR iOS 3+, NOT support HTML
- sendPush_PushIo() //Pending, FOR iOS 5+, Android, PAID (Free Trial)
- sendPush_PushOver() //Pending, FOR iOS 6+, Android, PAID
- sendPush_Whatsapp() //Pending
- sendPush_Line() //Pending
- sendPush_Hangouts() //Pending
- sendPush_WeChat() //Pending

http://www.reclipper.com/en/

/**/

?>
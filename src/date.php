<?
$hldetail = array();

$dates = @json_decode(@file_get_contents("./data/database/hksm.txt"), 1);

if( !empty($dates) ){
	foreach( $dates as $date ){
		if( isset($hldetail[ $date['serv_date_d'] ]) ){
			$hldetail[ $date['serv_date_d'] ] .= ", " . $date['slot_time'];
		}else{
			$hldetail[ $date['serv_date_d'] ] = $date['slot_time'];
		}
	}
}

?><!doctype html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<link rel="stylesheet" href="//code.jquery.com/ui/1.10.4/themes/smoothness/jquery-ui.css">
	<script src="//code.jquery.com/jquery-1.9.1.js"></script>
	<script src="//code.jquery.com/ui/1.10.4/jquery-ui.js"></script>
</head>

<body>
	<style>
		.canBook a{	background: pink !important;	}
	</style>
	
 	Available timeslot for booking: <div id="datepicker"></div>
	<br /><div id="dateDetail">Select the highlighted day to view detail</div>
	<script>
	var hldates = <?=json_encode(array_keys($hldetail))?>;
	var hldetail = <?=json_encode($hldetail)?>;
	
	Number.prototype.addZero = function() {
		return (this >= 10 ? this : '0' + this);
	}
	
	$(function() {
		$( "#datepicker" ).datepicker({
			numberOfMonths: 2,
			dateFormat: "yy-mm-dd",
			onSelect: function( a, b ) {
				$("#dateDetail").html("<b>" + a + "</b>: " + (hldetail[ a ] || 'No available timeslot for booking'));
			},
			beforeShowDay: function (date) {
				dmy = date.getFullYear() + '-' + (date.getMonth() + 1).addZero() + '-' + date.getDate().addZero();
				if ($.inArray(dmy, hldates) >= 0) {
					return [true, "canBook"];
				} else {
					return [true, ""];
				}
			}
		});
	});
	</script>
</body>
</html>